var webpack = require('webpack');

var path = require('path');

module.exports = {
	entry:{
		home:'./javascripts/main/home.js',
		user:'./javascripts/main/user.js',
		rgui:'./javascripts/main/rgui.js',
	},
	output:{
		path:'./javascripts/release',
		filename:'[name].js'
	},
	module:{
		loaders:[{
			test:/.js$/,
			exclude:/(node_modules|bower_components)/,
			loader:'babel',
			query:{
				compact:false,
				presets:['es2015']
			}
		}]
	}
};