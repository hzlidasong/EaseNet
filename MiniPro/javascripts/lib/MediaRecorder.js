const RecordRTC = require('./RecordRTC.js');

const EventEmitter = require('./EventEmitter.js');

/**
 * @description MediaRecorder构造函数为AudioRecorder与VideoRecorder提供基础功能
 * @extends {Function} EventEmitter 事件模拟器
 * @augments {Object} 基础配置内容
 */
class MediaRecorder extends EventEmitter {
    constructor(config) {
        super();
        this.config = Object.assign({
            // disableLogs:true,
        }, config);
        this.recorder = null;
        this.mediaStream = null;
    }
    /**
     * @param {Number} duration 录制最长时间，默认2分钟
     */
    setDuration(duration = 2 * 1000 * 1000) {
        if (duration) {
            this.recorder.setRecordingDuration(duration).onRecordingStopped((blobUrl) => {
                this.emit('stop', blobUrl);
                this.mediaStream.stop();
            });
        }
    }

    record() {
        this.captureUserMedia();
    }
    /**
     * @description 终止录制后向上层传递‘stop’事件
     */
    stopRecord() {
        this.recorder.stopRecording((blobUrl) => {
            this.emit('stop', blobUrl);
        });
        this.mediaStream.stop();
    }
    /**
     * @decription 录制完成后将录制内容传回，为上传到服务器
     * 做准备
     */
    post() {
        this.recorder.getDataURL((audioDataURL) => {
            var file = {
                blob: this.recorder.getBlob(),
                dataURL: audioDataURL
            };
            this.emit('post', file);
        });
    }
    /**
     * @description 兼容浏览器捕获音视频数据流接口
     * @return {Function} getUserMedia(统一的捕获数据接口)
     */
    getCompatibleUserMedia() {
        let browserMedia = [
            'getUserMedia',
            'webkitGetUserMedia',
            'mozGetUserMedia',
            'msGetUserMedia'
        ].find(function(media) {
            return !!navigator[media]
        });
        return navigator[browserMedia].bind(navigator);
    }

    captureUserMedia(onSucceedGainMedia = this.succeedGainMedia.bind(this), onFailMedia) {
        onFailMedia = onFailMedia || function() {};
        this.getCompatibleUserMedia()(this.config, onSucceedGainMedia, onFailMedia)
    }

    /**
     * 成功调用getUserMedia接口后的回调函数
     * @param  {Object} stream 捕获的数据流
     * @return {Undefined}
     */
    succeedGainMedia(stream) {
        this.mediaStream = stream;
        this.recorder = RecordRTC(stream, this.config);
        this.setDuration(this.config.duration);
        this.recorder.startRecording();
        this.emit('recording', stream);
    }
}

export {
    MediaRecorder
}