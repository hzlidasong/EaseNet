import{ MediaRecorder } from './MediaRecorder';

class VedioRecorder extends MediaRecorder{
	constructor(recordListens){
		super({
			type:'video',
			video:true,
			audio:true
		});
		this.init(recordListens);
	}

	init(recordListens){
		this.on('recording',recordListens.onRecording||this.onRecording);
		this.on('stop',this.post);
		this.on('post',recordListens.onPostToServer||this.onPostToServer);
	}

	onRecording(){
		console.log('recoding now ....');
	}

	onPostToServer(content){
		console.log(content);
	}
}

export { VedioRecorder }
