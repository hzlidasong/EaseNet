import{ AudioRecorder } from '../lib/AudioRecorder';

import{ VedioRecorder } from '../lib/VedioRecorder';

const $videoBtnStart = document.getElementById('video-start'),
	$audioBtnStart = document.getElementById('audio-start'),
	$videoRecorder = document.getElementById('video-recorder'),
	$audioRecorder = document.getElementById('audio-recorder');

var audioRecorder = new AudioRecorder({
	onRecording:function(){},
	onPostToServer:function (content){
		$audioRecorder.src = content['dataURL'];
		$audioRecorder.play();
	}
}).on('stop',function(){
	console.log('stopRecord');
});



$audioBtnStart.addEventListener('click',function(){
	if($audioBtnStart.isRecording){
		audioRecorder.stopRecord();
		$audioBtnStart.isRecording = false;
	}else{
		$audioBtnStart.isRecording = true;
		audioRecorder.record();
	}

});

$videoBtnStart.addEventListener('click',function(){
	if($videoBtnStart.isRecording){
		$videoBtnStart.isRecording = false;
		vedioRecorder.stopRecord();
	}else{
		$videoBtnStart.isRecording = true;
		vedioRecorder.record();
	}
});


var vedioRecorder = new VedioRecorder({
	onRecording:function(){},
	onPostToServer:function (content){
		$videoRecorder.src = content['dataURL'];
		$videoRecorder.play();
	}
});

