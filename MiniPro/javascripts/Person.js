// var RecordRTC = require('./lib/RecordRTC.js');

class Person {
    constructor(name) {
        this.name = name;
    }

    get(name) {
        return this[name];
    }

    set(name, value) {
        this[name] = value;
        return true;
    }

}


export { Person }