var gulp = require('gulp'),
	less = require('gulp-less'),
	webpack=require('webpack'),
	webpackConfig = require('./webpack.config.js'),
	WebpackDevServer = require('webpack-dev-server'),
	mcss = require('gulp_mcss');

gulp.task('less2css',function(){
	gulp.src('less/main/*.less')
		.pipe(less())
		.pipe(gulp.dest('css'));
});

gulp.task('watchLess',function(){
	gulp.watch(['less/main/*.less','less/*.less'],['less2css']);
});


// gulp.task('webpack-dev-server',function(){
// 	var compiler = webpack(webpackConfig);
// 	new WebpackDevServer(compiler).listen(8080);
// });
// 
gulp.task('mcss2css',function(){
	gulp.src('./mcss/*.mcss')
	.pipe(mcss({
		importCss:true
	}))
	.pipe(gulp.dest('./css/'))
});


gulp.task('watchMcss',function(){
	gulp.watch(['mcss/*.mcss','mcss/modules/*.mcss'],['mcss2css']);
});
